module Database.PostgreSQL.Typed.Expression.Table where

import qualified Data.Map.Strict as M
import           Data.Singletons
import           Data.Singletons.Prelude.List
import           Database.PostgreSQL.Query
import           Database.PostgreSQL.Simple.Types
import           Database.PostgreSQL.Typed.Expression.Value
import           Database.PostgreSQL.Typed.Table
import           GHC.TypeLits
import           TypeFun.Data.List

--  FIXME: Turn mandatory constraint back
-- | Table expression which is either join of some tables or just
-- table. Subqueries are not supported because fuck you thats why.
data TableExp (ts :: [Table]) where
  TEJoin  :: Join ts -> TableExp ts
  TETable :: Sing table
          -> TableExp '[table]

tbl :: forall proxy pre tbl col
     . (KnownSymbol pre, KnownSymbol tbl)
    => proxy '(pre, tbl, col)
    -> TableExp '[ 'Table 'Mnd pre tbl]
tbl _ = TETable p
  where
    p = sing :: Sing ('Table 'Mnd pre tbl)

crossJoin :: Sing (th ': tt) -> TableExp (th ': tt)
crossJoin = \case
  SCons th SNil        -> TETable th
  SCons th (SCons a b) -> TEJoin $ JCross (TETable th) (crossJoin (SCons a b))

renderTableExp :: SynonymsMap -> TableExp ts -> SqlBuilder
renderTableExp sm = \case
  TEJoin join -> renderJoin sm join
  TETable p   -> [sqlExp|^{tblName} AS ^{tblSyn}|]
    where
      tblName = getTableName p
      tblSyn = case M.lookup (getTableId p) sm of
        Nothing -> error "Synonym not found, this is a bug!"
        Just s  -> Identifier s

jImplicit
  :: (SingI '[t1, t2])
  => Expression '[t1, t2] Bool
  -> TableExp '[t1, t2]
jImplicit = TEJoin . JImplicit

--  FIXME: tables from left and right part should not intersect
--  FIXME: add inner join for convenience?
data Join (ts :: [Table]) where
  JImplicit
    :: forall (t1 :: Table) (t2 :: Table)
     . (SingI '[t1, t2])
    => Expression '[t1, t2] Bool
    -> Join '[t1, t2]
  -- ^ Implicit join of just two tables. Expression becomes condition and tables
  -- are got from expressions type. If both tables are optional then result will
  -- be full outer join. If both tables are manadatory then result will be inner
  -- join. If one of tables is optional and other is mandatory then result will
  -- be left join. This happening dynamically on rendering.
  JAppend
    :: ('[t] ~ (Substract etbls tbls))
    => TableExp tbls
    -> Sing t
    -> Expression etbls Bool
    -> Join (t ': tbls)
  -- ^ Appends exactly one table to join. Gets table from expression. If gotten
  -- table is optional then it rendered to left join, otherwise to inner
  -- join. Expression can use any number of tables from left part of join plus
  -- exatly one new table
  JCross
    :: TableExp l
    -> TableExp r
    -> Join (l :++: r)
  -- ^ Trivial cross join of two any table expressions.
  JInner
    :: TableExp l
    -> TableExp r
    -> SomeExpression (l :++: r) Bool
    -> Join (l :++: r)
  -- ^ Inner join of two any table expressions
  JLeft
    :: TableExp l
    -> TableExp r
    -> SomeExpression (l :++: r) Bool
    -> Join (l :++: (MakeOptional r))
  JOuter
    :: TableExp l
    -> TableExp r
    -> SomeExpression (l :++: r) Bool
    -> Join ((MakeOptional l) :++: (MakeOptional r))

renderJoin
  :: forall ts
   . SynonymsMap
  -> Join ts
  -> SqlBuilder
renderJoin smap = \case
  JCross tl tr -> [sqlExp|
    (^{renderTableExp smap tl}) CROSS JOIN (^{renderTableExp smap tr})|]
  JImplicit expr -> case sing :: (Sing ts) of
    (SCons t1 (SCons t2 SNil)) ->
      let te1 = renderTableExp smap $ TETable t1
          te2 = renderTableExp smap $ TETable t2
          join :: SqlBuilder
          join = case (_ttOptional $ fromSing t1, _ttOptional $ fromSing t2) of
            (Mnd, Mnd) -> [sqlExp|(^{te1}) INNER JOIN (^{te2})|]
            (Opt, Opt)   -> [sqlExp|(^{te1}) FULL OUTER JOIN (^{te2})|]
            (Mnd, Opt)  -> [sqlExp|(^{te1}) LEFT OUTER JOIN (^{te2})|]
            (Opt, Mnd)  -> [sqlExp|(^{te2}) RIGHT OUTER JOIN (^{te1})|]
      in [sqlExp|^{join} ON (^{renderExpression smap expr})|]
    _ -> error "IMPOSIBUR"
  JAppend tbls t expr ->
    let joinType :: SqlBuilder
        joinType = case _ttOptional $ fromSing t of
          Opt -> "LEFT OUTER JOIN"
          Mnd -> "INNER JOIN"
        te = TETable t
    in [sqlExp|(^{renderTableExp smap tbls}) ^{joinType} (^{renderTableExp smap te})
               ON (^{renderExpression smap expr})|]
  JInner tl tr cond -> [sqlExp|
    (^{renderTableExp smap tl}) INNER JOIN (^{renderTableExp smap tr})
    ON (^{renderSomeExpression smap cond})|]
  JLeft tl tr (Narrowed cond) -> [sqlExp|
    (^{renderTableExp smap tl}) LEFT OUTER JOIN (^{renderTableExp smap tr})
    ON (^{renderExpression smap cond})|]
  JOuter tl tr (Narrowed cond) -> [sqlExp|
    (^{renderTableExp smap tl}) FULL OUTER JOIN (^{renderTableExp smap tr})
    ON (^{renderExpression smap cond})|]
