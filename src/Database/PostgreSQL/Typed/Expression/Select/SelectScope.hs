module Database.PostgreSQL.Typed.Expression.Select.SelectScope where

import Control.Lens
import Data.Singletons
import Database.PostgreSQL.Query
import Database.PostgreSQL.Typed.Expression.Select.GroupBy
import Database.PostgreSQL.Typed.Expression.Table
import Database.PostgreSQL.Typed.Expression.Value
import Database.PostgreSQL.Typed.Table
import TypeFun.Data.List

-- | Tables + conditions + limit/offset order by and such stuff
data SelectScope (tbls :: [Table]) where
  SelectScope ::
    { _scTables    :: TableExp tbls
       -- ^ Tables expression
    , _scCondition :: SomeExpression tbls Bool
       -- ^ Expression with condition to filter. Condition expressions must
       -- not know is table mandatory or optional for composability and SQL
       -- will handle that for us anyway.
    , _scGroupBy   :: GroupBy tbls
    -- ^ Group by expression
    , _scOrderBy   :: Hidden (SomeExpressionList tbls)
    -- ^ Order by expression
    , _scLimit     :: Maybe Int
    -- ^ Limit
    , _scOffset    :: Maybe Int
    -- ^ Offset
    } -> SelectScope tbls

-- makeLenses ''SelectScope

scTables
  :: forall t1 t2
   . (SubList t1 t2)
  => Lens (SelectScope t1) (SelectScope t2) (TableExp t1) (TableExp t2)
scTables = lens _scTables s
  where
    s :: SelectScope t1 -> TableExp t2 -> SelectScope t2
    s sc tbls = sc
      { _scTables    = tbls
      , _scCondition = expand $ _scCondition sc
      , _scGroupBy   = expandGroupBy $ _scGroupBy sc
      , _scOrderBy   = expandHidden $ _scOrderBy sc
      }

scTables' :: Lens' (SelectScope t) (TableExp t)
scTables' = lens _scTables s
  where
    s sc tbls = sc { _scTables = tbls }

scCondition' :: Lens' (SelectScope t) (SomeExpression t Bool)
scCondition' = lens _scCondition s
  where
    s sc cond = sc { _scCondition = cond }

scGroupBy' :: Lens' (SelectScope t) (GroupBy t)
scGroupBy' = lens _scGroupBy s
  where
    s sc gb = sc { _scGroupBy = gb }

scLimit' :: Lens' (SelectScope t) (Maybe Int)
scLimit' = lens _scLimit s
  where
    s sc sl = sc { _scLimit = sl }

scOffset' :: Lens' (SelectScope t) (Maybe Int)
scOffset' = lens _scOffset s
  where
    s sc off = sc { _scOffset = off }

class ToSelectScope a where
  type ScopeTables a :: [Table]
  toSelectScope :: a -> SelectScope (ScopeTables a)

instance ToSelectScope (SelectScope tbls) where
  type ScopeTables (SelectScope tbls) = tbls
  toSelectScope = id

instance forall tbls t ts
         . ( SingI tbls, tbls ~ MakeMandatory tbls
           , SubList tbls tbls , tbls ~ (t ': ts) )
         => ToSelectScope (Expression tbls Bool) where
  type ScopeTables (Expression tbls Bool) = tbls
  toSelectScope expr = SelectScope
    { _scTables = texpr
    , _scCondition = Narrowed expr
    , _scGroupBy = noGroupBy
    , _scOrderBy = Hidden $ Narrowed ELNil
    , _scLimit = Nothing
    , _scOffset = Nothing }
    where
      texpr = crossJoin (sing :: Sing tbls)

instance ToSelectScope (TableExp tbls) where
  type ScopeTables (TableExp tbls) = tbls
  toSelectScope tbls = SelectScope
    { _scTables    = tbls
    , _scCondition = Narrowed $ litE True
    , _scGroupBy   = noGroupBy
    , _scOrderBy   = Hidden $ Narrowed ELNil
    , _scLimit     = Nothing
    , _scOffset    = Nothing }


renderSelectScope :: SynonymsMap -> SelectScope tbls -> SqlBuilder
renderSelectScope synonyms (SelectScope tblsexp cond grpby ordby lim offs) =
  [sqlExp|FROM ^{tablesb} WHERE ^{condb} ^{groupb} ^{ordb} ^{limb} ^{offsb}|]
  where
    commaEL :: SomeExpressionList tbls typs -> SqlBuilder
    commaEL el = commaSep $ renderSomeExpressionList synonyms el
    tablesb  = renderTableExp synonyms tblsexp
    condb    = renderSomeExpression synonyms cond
    groupb = case renderGroupBy synonyms grpby of
      Nothing -> mempty
      Just grp -> [sqlExp|GROUP BY ^{grp}|]
    ordb = case ordby of
      Hidden (Narrowed ELNil) -> mempty
      Hidden ord              ->
        [sqlExp|ORDER BY ^{commaEL ord}|]
    limb = case lim of
      Nothing -> mempty
      Just l -> [sqlExp|LIMIT #{l}|]
    offsb = case offs of
      Nothing -> mempty
      Just o -> [sqlExp|OFFSET #{o}|]
