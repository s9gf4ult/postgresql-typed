module Database.PostgreSQL.Typed.Expression.Select.Select where

import Control.Lens
import Data.Singletons
import Database.PostgreSQL.Query
import Database.PostgreSQL.Typed.Expression.Select.SelectScope
import Database.PostgreSQL.Typed.Expression.Value
import Database.PostgreSQL.Typed.Table
import TypeFun.Data.List

data Select (tbls :: [Table]) (typs :: [*]) where
  Select :: (SingI otables) =>
    { _sReturned :: SomeExpressionList otables typs
       -- ^ Expressions to return
    , _sScope    :: SelectScope otables
    } -> Select otables typs

toSelect
  :: ( ToSelectScope scope
     , SingI (ScopeTables scope)
     , SubList rtables (ScopeTables scope) )
  => ExpressionList rtables typs
  -> scope
  -> Select (ScopeTables scope) typs
toSelect el scope = Select (Narrowed el) $ toSelectScope scope

sScope
  :: (SubList tbls1 tbls2, SingI tbls2)
  => Lens (Select tbls1 typs) (Select tbls2 typs)
     (SelectScope tbls1) (SelectScope tbls2)
sScope = lens _sScope s
  where
    s sl sc = Select
      { _sReturned = expand $ _sReturned sl
      , _sScope    = sc }

sScope' :: Lens' (Select tbls typs) (SelectScope tbls)
sScope' = lens _sScope s
  where
    s sl sc = sl {_sScope = sc}

sReturned :: Lens (Select tbls typs1) (Select tbls typs2)
             (SomeExpressionList tbls typs1)
             (SomeExpressionList tbls typs2)
sReturned = lens _sReturned s
  where
    s sl ret = sl {_sReturned = ret}

sReturned' :: Lens' (Select tbls typs) (SomeExpressionList tbls typs)
sReturned' = sReturned

renderSelect
  :: forall tbls typs
   . Select tbls typs -> SqlBuilder
renderSelect (Select erows scope) =
  [sqlExp|SELECT ^{erowsb} ^{renderSelectScope synonyms scope}|]
  where
    erowsb   = commaSep $ renderSomeExpressionList synonyms erows
    synonyms = genSynonyms (sing :: Sing tbls)
