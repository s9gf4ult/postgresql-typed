module Database.PostgreSQL.Typed.Expression.Select.GroupBy where

import Control.Lens
import Database.PostgreSQL.Query
import Database.PostgreSQL.Typed.Expression.Value
import Database.PostgreSQL.Typed.Table
import TypeFun.Data.List

data GroupBy otables where
  GroupBy ::
    { _gbGroupBy :: Hidden (SomeExpressionList otables)
                    -- ^ Expression to group by
    , _gbHaving  :: SomeExpression otables Bool
                    -- ^ Condition for HAVING part
    } -> GroupBy otables

expandGroupBy :: (SubList tbls1 tbls2) => GroupBy tbls1 -> GroupBy tbls2
expandGroupBy gb = GroupBy
  { _gbGroupBy = expandHidden $ _gbGroupBy gb
  , _gbHaving  = expand $ _gbHaving gb }

gbGroupBy
  :: (SubList tbls1 tbls2)
  => Lens (GroupBy tbls1) (GroupBy tbls2)
     (Hidden (SomeExpressionList tbls1))
     (Hidden (SomeExpressionList tbls2))
gbGroupBy = lens _gbGroupBy s
  where
    s gb exps = gb
      { _gbGroupBy = exps
      , _gbHaving  = expand $ _gbHaving gb }

gbGroupBy' :: Lens' (GroupBy t) (Hidden (SomeExpressionList t))
gbGroupBy' = lens _gbGroupBy s
  where
    s gb exps = gb { _gbGroupBy = exps }

gbHaving
  :: (SubList tbls1 tbls2)
  => Lens (GroupBy tbls1) (GroupBy tbls2)
     (SomeExpression tbls1 Bool) (SomeExpression tbls2 Bool)
gbHaving = lens _gbHaving s
  where
    s gb hvng = gb
      { _gbGroupBy = expandHidden $ _gbGroupBy gb
      , _gbHaving  = hvng }

gbHaving' :: Lens' (GroupBy t) (SomeExpression t Bool)
gbHaving' = lens _gbHaving s
  where
    s gb hvng = gb { _gbHaving = hvng }

noGroupBy :: GroupBy tbls
noGroupBy = GroupBy (Hidden $ Narrowed ELNil) $ Narrowed $ litE True

justGroupBy :: SomeExpressionList otables typs -> GroupBy otables
justGroupBy el = GroupBy (Hidden el) $ Narrowed $ litE True

renderGroupBy :: SynonymsMap -> GroupBy tbls -> Maybe SqlBuilder
renderGroupBy sm (GroupBy (Hidden el) cond) = case el of
  (Narrowed ELNil) -> Nothing
  _ -> Just [sqlExp|
    ^{commaSep $ renderSomeExpressionList sm el}
    HAVING ^{renderSomeExpression sm cond}|]
