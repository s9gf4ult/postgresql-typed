module Database.PostgreSQL.Typed.Expression.Select.ScopeBuilder where

import Control.Lens
import Data.Singletons
import Database.PostgreSQL.Typed.Expression.Select.GroupBy
import Database.PostgreSQL.Typed.Expression.Select.SelectScope
import Database.PostgreSQL.Typed.Expression.Table
import Database.PostgreSQL.Typed.Expression.Value
import Database.PostgreSQL.Typed.Table
import TypeFun.Data.List

-- | Simple builder type to collect condition together with tables used in. This
-- type is not intended to be used directly, you must construct it through
-- 'ToScopeBuilder' methods instead.
data ScopeBuilder (jtables :: [Table]) (ctables :: [Table]) where
  ScopeBuilder ::
    { _sbTable     :: TableExp jtables
    , _sbCondition :: Expression ctables Bool
    } -> ScopeBuilder jtables ctables

makeLenses ''ScopeBuilder

instance (SubList c j)
         => ToSelectScope (ScopeBuilder j c) where
  type ScopeTables (ScopeBuilder j c) = j
  toSelectScope bld = SelectScope
    { _scTables    = bld ^. sbTable
    , _scCondition = Narrowed $ bld ^. sbCondition
    , _scGroupBy   = noGroupBy
    , _scOrderBy   = Hidden $ Narrowed $ ELNil
    , _scLimit     = Nothing
    , _scOffset    = Nothing
    }

class ToScopeBuilder a where
  type SBJoinTables a :: [Table]
  type SBCondTables a :: [Table]
  toScopeBuilder :: a -> ScopeBuilder (SBJoinTables a) (SBCondTables a)

instance ToScopeBuilder (ScopeBuilder j c) where
  type SBJoinTables (ScopeBuilder j c) = j
  type SBCondTables (ScopeBuilder j c) = c
  toScopeBuilder = id

instance ToScopeBuilder (TableExp j) where
  type SBJoinTables (TableExp j) = j
  type SBCondTables (TableExp j) = '[]
  toScopeBuilder t = ScopeBuilder t (litE True)

instance (SingI c, c ~ (t ': ts)) => ToScopeBuilder (Expression c Bool) where
  type SBJoinTables (Expression c Bool) = c
  type SBCondTables (Expression c Bool) = c
  toScopeBuilder ex = ScopeBuilder (crossJoin (sing :: Sing c)) ex


-- | Append to scope just one table. Either optional or mandatory
sbAppend
  :: forall tbl btbls atbls  cond
   . ('[tbl] ~ (Substract btbls atbls), SingI tbl)
  => ScopeBuilder atbls cond
  -> Expression btbls Bool
  -> ScopeBuilder (tbl ': atbls) cond
sbAppend scope expr = over sbTable modifyCond scope
  where
    modifyCond :: TableExp atbls -> TableExp (tbl ': atbls)
    modifyCond te = TEJoin $ JAppend te sing expr

(#+#) :: ( ToScopeBuilder scope
         , atbls ~ SBJoinTables scope, '[tbl] ~ (Substract btbls atbls)
         , SingI tbl )
      => scope
      -> Expression btbls Bool
      -> ScopeBuilder (tbl ': atbls) (SBCondTables scope)
a #+# expr = sbAppend (toScopeBuilder a) expr

infixl 3 #+#
