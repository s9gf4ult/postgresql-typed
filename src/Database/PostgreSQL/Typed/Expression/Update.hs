module Database.PostgreSQL.Typed.Expression.Update where

import qualified Data.List as L
import           Data.Singletons
import           Data.Singletons.Prelude.List
import           Database.PostgreSQL.Query
import           Database.PostgreSQL.Simple.Types
import           Database.PostgreSQL.Typed.Expression.ColumnValue
import           Database.PostgreSQL.Typed.Expression.Value
import           Database.PostgreSQL.Typed.Table

-- | Update returning query
data Update (tbl :: Table) (typs :: [*]) where
  Update :: (tbl ~ ('Table 'Mnd pre tbln))
         => Sing tbl
            -- ^ Table to update
         -> ColumnValues tbl ucols
            -- ^ Columns and values to set to
         -> SomeExpression '[tbl] Bool
            -- ^ @WHERE@ Condition
         -> SomeExpressionList '[tbl] typs
            -- ^ @RETURNING@ expression list
         -> Update tbl typs

renderUpdate :: Update tbl typs -> SqlBuilder
renderUpdate (Update tbl cols (Narrowed cond) (Narrowed ret)) =
  [sqlExp|UPDATE ^{tableb} SET ^{setb} WHERE ^{condb} ^{retb}|]
  where
    tableb = [sqlExp|^{getTableName tbl} AS ^{tsyn}|]
    tsyn = Identifier $ lookupSyn (getTableId tbl) synonyms
    setb = mconcat $ L.intersperse ", " $ renderColumnSets cols
    condb = renderExpression synonyms cond
    retb = case ret of
      ELNil -> mempty
      x -> [sqlExp|RETURNING ^{mconcat $ L.intersperse "," $ renderExpressionList synonyms x}|]
    synonyms = genSynonyms (SCons tbl SNil)
