module Database.PostgreSQL.Typed.Expression.TH where

import           Data.Monoid
import           Data.Proxy
import qualified Data.Text as T
import           Database.PostgreSQL.Typed.Table
import           Language.Haskell.TH
import           Language.Haskell.TH.Quote

expQuoter :: (String -> Q Exp) -> QuasiQuoter
expQuoter e = QuasiQuoter
  { quoteExp = e
  , quoteDec = error "Wrong usage"
  , quotePat = error "Wrong usage"
  , quoteType = error "Wrong usage" }

-- | Generates @Proxy :: Proxy '(opt, pre, tbl, col)@ for functions like 'ecol' to
-- write reference to table or column in table with ease.
c :: QuasiQuoter
c = expQuoter $ \(T.pack -> pat) -> do
  let
    (pref, tblcol) = case T.splitOn "|" pat of
      [tbl]      -> ("", st tbl)
      [pre, tbl] -> (st pre, st tbl)
      _ -> error "Got too many '|' symbols"
    (otbln, coln) = case T.splitOn "." tblcol of
      [tbl] -> (st tbl, "")
      [tbl, col] -> (st tbl, st col)
      [schema, tbl, col] -> ((st schema) <> "." <> (st tbl), col)
      _ -> error "Too many dots got, allow just 1 or 2"
    (opt, tbln) = case T.uncons otbln of
      Nothing         -> error "Empty table name, are you serious?"
      Just ('?', tbl) -> ([t|'Opt|], tbl)
      Just (ch, tbl)   -> ([t|'Mnd|], T.cons ch tbl)
  [e|Proxy :: Proxy '($(opt), $(slt pref), $(slt tbln), $(slt coln))|]
  where
    st = T.strip
    slt = litT . strTyLit . T.unpack
