module Database.PostgreSQL.Typed.Expression.Value.Render where

import qualified Data.List as L
import           Data.Singletons
import qualified Data.Text as T
import           Database.PostgreSQL.Query
import           Database.PostgreSQL.Simple.Types
import           Database.PostgreSQL.Typed.Expression.Value.Types
import           Database.PostgreSQL.Typed.Table

renderExpression
  :: SynonymsMap
  -> Expression ctables typ
  -> SqlBuilder
renderExpression sm = \case
  ECol tbl col -> toSqlBuilder $ QualifiedIdentifier (Just syn) colname
    where
      syn = lookupSyn tid sm
      tid = getTableId tbl
      colname = T.pack $ fromSing col
  EComb el render -> render $ renderExpressionList sm el

renderExpressionList
  :: SynonymsMap
  -> ExpressionList ctables typs
  -> [SqlBuilder]
renderExpressionList sm = \case
  ELNil -> []
  e ::& el -> renderExpression sm e : renderExpressionList sm el

renderSomeExpression
  :: SynonymsMap
  -> SomeExpression tbls typ
  -> SqlBuilder
renderSomeExpression sm (Narrowed e) =
  renderExpression sm e

renderSomeExpressionList
  :: SynonymsMap
  -> SomeExpressionList tbls typs
  -> [SqlBuilder]
renderSomeExpressionList sm (Narrowed e) =
  renderExpressionList sm e

commaSep :: [SqlBuilder] -> SqlBuilder
commaSep = mconcat . L.intersperse ", "

-- | Render restricted expression case where we work with just one table
renderSomeExpressionUnqual :: SomeExpression '[tbl] typ -> SqlBuilder
renderSomeExpressionUnqual = error "FIXME: not implemented"

-- | Special case of rendering where only one table is in scope of expression
-- and synonyms are not posible (like inside @INSERT@)
renderSomeExpressionListUnqual :: SomeExpressionList '[tbl] typs -> [SqlBuilder]
renderSomeExpressionListUnqual = error "FIXME: not implemented"
