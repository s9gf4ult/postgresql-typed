{-# LANGUAGE AllowAmbiguousTypes #-}

module Database.PostgreSQL.Typed.Expression.Value.Types where

import Data.Singletons
import Data.Typeable
import Database.PostgreSQL.Query
import Database.PostgreSQL.Typed.Table
import TypeFun.Data.List
import Unsafe.Coerce (unsafeCoerce)

data Narrowed (e :: [k] -> t -> *) (rst :: [k]) (typ :: t) :: * where
  Narrowed
    :: (SubList clist olist)
    => e clist t
    -> Narrowed e olist t

expand
  :: forall e tbls1 tbls2 t
   . (SubList tbls1 tbls2)
  => Narrowed e tbls1 t
  -> Narrowed e tbls2 t
expand (Narrowed e) = sublistTransitive (p1 e) p2 p3 $ Narrowed e
  where
    p1 :: e ctbls t -> Proxy ctbls
    p1 _ = Proxy
    p2 = Proxy :: Proxy tbls1
    p3 = Proxy :: Proxy tbls2

data Hidden (e :: a -> *) :: * where
  Hidden :: forall e a. e a -> Hidden e

expandHidden
  :: forall e tbls1 tbls2
   . (SubList tbls1 tbls2)
  => Hidden (Narrowed e tbls1)
  -> Hidden (Narrowed e tbls2)
expandHidden (Hidden n) = Hidden $ expand n

-- | SQL Expression of type 't' using tables 'ctables' and working in context
-- with 'otables' accepatable.
data Expression (ctables :: [Table]) (t :: *) where
  ECol :: Sing table
       -> Sing col
       -> Expression '[table] (ColExprType table col)
  -- ^ Colmn expression. It is rendered to just column reference. Constructor is
  -- needed to constraint types and pull tables to table list

  EComb :: ExpressionList ctables typs
        -> ([SqlBuilder] -> SqlBuilder)
        -> Expression ctables typ
  -- ^ Any arbitrarily SQL expression combined from other expressions or just a
  -- constant (in case of empty list as first arg). The output type is
  -- constrained by smart constructor.

type SomeExpression = Narrowed Expression


--  FIXME: UnionList is not proper, use something failing at compile time when
--  same tables with different optionalities are met
-- | List of expressions using tables 'otables' returning list of types 'typs'
data ExpressionList (ctables :: [Table]) (typs :: [*]) where
  ELNil :: ExpressionList '[] '[]
  (::&)
    :: Expression ct1 typ
    -> ExpressionList ct2 typs
    -> ExpressionList (Union ct2 ct1) (typ ': typs)

infixr 4 ::&

type SomeExpressionList = Narrowed ExpressionList

elconcat
  :: forall otables t1 t2
   . SomeExpressionList otables t1
  -> SomeExpressionList otables t2
  -> SomeExpressionList otables (t1 :++: t2)
elconcat (Narrowed el1) (Narrowed el2) = case el1 of
  ELNil       -> Narrowed el2
  eh1 ::& et1 -> trustMe et1 eh1 (Proxy :: Proxy otables)
    $ elcons (Narrowed eh1) (elconcat (Narrowed et1) (Narrowed el2))

elcons
  :: forall otables typ typs
   . SomeExpression otables typ
  -> SomeExpressionList otables typs
  -> SomeExpressionList otables (typ ': typs)
elcons (Narrowed e) (Narrowed el) =
  beleiveMe el e (Proxy :: Proxy otables) $ Narrowed $ e ::& el

trustMe
  :: (SubList (Union alist blist) olist)
  => proxy alist a -> proxy2 blist b -> proxy3 olist
  -> ((SubList alist olist, SubList blist olist) => r)
  -> r
trustMe _ _ _ = unsafeCoerce id

beleiveMe
  :: (SubList alist olist, SubList blist olist)
  => proxy1 alist a -> proxy2 blist b -> proxy3 olist
  -> ((SubList (Union alist blist) olist) => r)
  -> r
beleiveMe _ _ _ = unsafeCoerce id


sublistTransitive
  :: (SubList a b, SubList b c)
  => proxy1 a
  -> proxy2 b
  -> proxy3 c
  -> (SubList a c => r)
  -> r
sublistTransitive _ _ _ = unsafeCoerce id
