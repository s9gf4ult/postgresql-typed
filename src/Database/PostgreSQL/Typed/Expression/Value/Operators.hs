module Database.PostgreSQL.Typed.Expression.Value.Operators where

import Data.Singletons
import Data.Text (Text)
import Database.PostgreSQL.Query
import Database.PostgreSQL.Typed.Expression.Value.Types
import Database.PostgreSQL.Typed.Table
import GHC.TypeLits
import TypeFun.Data.List

bifix
  :: SqlBuilder
  -> Expression ct1 t1
  -> Expression ct2 t2
  -> Expression (Union ct2 ct1) t3
bifix btw e1 e2 = EComb (e1 ::& e2 ::& ELNil) render
  where
    render = \case
      [l, r] -> [sqlExp|(^{l}) ^{btw} (^{r})|]
      _ -> error $ "got wrong builders list"


col :: forall proxy opt pre tbl col ctable
     . ( KnownSymbol pre, KnownSymbol tbl, KnownSymbol col
       , ctable ~ ('Table opt pre tbl), SingI ctable )
    => proxy '(opt, pre, tbl, col)
    -> Expression '[ctable] (ColExprType ctable col)
col _ = ECol tblp colp
  where
    tblp = sing :: Sing ctable
    colp = sing :: Sing col

(&&.),(||.)
  :: Expression ct1 Bool
  -> Expression ct2 Bool
  -> Expression (Union ct2 ct1) Bool
e1 &&. e2 = bifix "AND" e1 e2
e1 ||. e2 = bifix "OR" e1 e2

infixr 6 &&.
infixr 5 ||.

(==.),(/=.)
  :: ( SquashMaybe t1 ~ SquashMaybe t2)
  => Expression ct1 t1
  -> Expression ct2 t2
  -> Expression (Union ct2 ct1) Bool
a ==. b = bifix "=" a b
a /=. b = bifix "!=" a b

infix 7 ==.
infix 7 /=.

litE :: (ToField a) => a -> Expression '[] a
litE a = EComb ELNil (const $ mkValue a)

textE :: Text -> Expression '[] Text
textE = litE

likeE
  :: (SquashMaybe t1 ~ SquashMaybe t2)
  => Expression ct1 t1
  -> Expression ct2 t2
  -> Expression (Union ct2 ct1) Bool
likeE = bifix "LIKE"

infix 7 `likeE`

isNullE :: Expression c t -> Expression c Bool
isNullE e = EComb (e ::& ELNil) render
  where
    render = \case
      [a] -> [sqlExp|(^{a}) IS NULL|]
      _   -> error "got wrong count of arguments, this is a bug!"

isNotNullE :: Expression c t -> Expression c Bool
isNotNullE e = EComb (e ::& ELNil) render
  where
    render = \case
      [a] -> [sqlExp|(^{a}) IS NOT NULL|]
      _   -> error "got wrong count of arguments, this is a bug!"

countE :: (Num i) => Expression c t -> Expression c i
countE e = EComb (e ::& ELNil) render
  where
    render = \case
      [a] -> [sqlExp|count(^{a})|]
      _   -> error "got wrong count of arguments, this is a bug!"
