module Database.PostgreSQL.Typed.Expression.ColumnValue where

import           Control.Lens (Lens', lens)
import           Data.Singletons
import qualified Data.Text as T
import           Data.Vinyl
import           Data.Vinyl.Functor
import           Database.PostgreSQL.Query
import           Database.PostgreSQL.Simple.Types
import           Database.PostgreSQL.Typed.Expression.Value
import           Database.PostgreSQL.Typed.Table
import           GHC.TypeLits

type ColumnValues (tbl :: Table) (cols :: [Symbol]) =
  Rec (ColumnValue tbl) cols

data ColumnValue (tbl :: Table) (col :: Symbol) where
  ColumnValue ::
    { _cvColumn :: Sing col
    , _cvValue  :: SomeExpression '[tbl] (ColExprType tbl col)
    } -> ColumnValue tbl col

cvColumn' :: Lens' (ColumnValue tbl col) (Sing col)
cvColumn' = lens g s
  where
    g = _cvColumn
    s cv c = cv { _cvColumn = c }

cvValue' :: Lens' (ColumnValue tbl col) (SomeExpression '[tbl] (ColExprType tbl col))
cvValue' = lens g s
  where
    g = _cvValue
    s cv val = cv { _cvValue = val }

renderColumnValues :: ColumnValues tbl cols -> [SqlBuilder]
renderColumnValues = recordToList . rmap (Const . renderColumnValue)

renderColumnSets :: ColumnValues tbl cols -> [SqlBuilder]
renderColumnSets = recordToList . rmap (Const . renderColumnSet)

renderColumnValue :: ColumnValue tbl col -> SqlBuilder
renderColumnValue (ColumnValue _ expr) = renderSomeExpressionUnqual expr

renderColumnSet :: forall tbl col
                 . ColumnValue tbl col
                -> SqlBuilder
renderColumnSet (ColumnValue c expr) = [sqlExp|^{colName} = ^{exprb}|]
  where
    colName = Identifier $ T.pack $ fromSing c
    exprb = renderSomeExpressionUnqual expr
