module Database.PostgreSQL.Typed.Expression.Insert where

import Data.Foldable
import Data.List.NonEmpty (NonEmpty(..))
import Data.Singletons
import Database.PostgreSQL.Query
import Database.PostgreSQL.Typed.Expression.ColumnValue
import Database.PostgreSQL.Typed.Expression.Value
import Database.PostgreSQL.Typed.Table

data Insert (table :: Table) (typs :: [*]) where
  Insert :: ( )
         => Sing table
            -- ^ Table to insert into
         -> Sing cols
         -> NonEmpty (ColumnValues table cols)
            -- ^ what to insert, at least one record
         -> SomeExpressionList '[table] typs
            -- ^ what to return in @RETURNING@ clause
         -> Insert table typs

renderInsert :: Insert table typs -> SqlBuilder
renderInsert (Insert tbl cols cvals rets) = [sqlExp|
  INSERT INTO ^{getTableName tbl}(^{colNames}) VALUES ^{valuesb} ^{returningb}|]
  where
    colNames = commaSep $ map toSqlBuilder $ getColNames cols
    valuesb = commaSep $ map comabrace $ toList cvals
    comabrace vls = [sqlExp|(^{commaSep $ renderColumnValues vls})|]
    returningb :: SqlBuilder
    returningb = case rets of
      (Narrowed ELNil) -> mempty
      (Narrowed _) ->
        [sqlExp|RETURNING ^{commaSep $ renderSomeExpressionListUnqual rets}|]
