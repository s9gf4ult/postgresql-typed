module Database.PostgreSQL.Typed.Table where

import qualified Data.Map.Strict as M
import           Data.Proxy
import           Data.Singletons
import           Data.Singletons.Prelude.List
import           Data.Singletons.TH
import           Data.Singletons.TypeLits
import           Data.String
import           Data.Text (Text)
import qualified Data.Text as T
import           Database.PostgreSQL.Simple.Types

singletons [d|data Optionality = Opt | Mnd|]

type family ColType (t :: Symbol) (c :: Symbol) :: *

type family SquashMaybe (ma :: *) :: * where
  SquashMaybe (Maybe a) = SquashMaybe a
  SquashMaybe a         = a

-- | Returns expression type considering nullability. @tbl@ must be always
-- 'Table' type.
type family ColExprType (tbl :: Table) (c :: Symbol) :: * where
  ColExprType ('Table 'Opt pre tn) col =
    Maybe (SquashMaybe (ColType tn col))
  ColExprType ('Table 'Mnd pre tn) col =
    ColType tn col

-- | Indexed type to represent table with prefix. Prefixes are just strings
-- making table uniq. First optionality is the optionality flag. This parameter
-- is required to tell if all fields of the table are optional (in case of outer
-- join)
data Table = Table Optionality Symbol Symbol

-- | Term level table information carrier.
data TTable = TTable
  { _ttOptional  :: Optionality
  , _ttPrefix    :: String
  , _ttTableName :: String
  }

data instance Sing (tbl :: Table) where
  STable :: Sing opt -> Sing pre -> Sing tbl -> Sing ('Table opt pre tbl)

instance ( SingI opt, SingI pre, SingI tbl )
         => SingI ('Table opt pre tbl) where
  sing = STable sing sing sing

instance SingKind ('KProxy :: KProxy Table) where
  type DemoteRep ('KProxy :: KProxy Table) = TTable

  fromSing = \case
    (STable opt pre tbl) ->
      TTable (fromSing opt) (fromSing pre) (fromSing tbl)

  toSing (TTable opt' pre' tbl') = case toSing opt' of
    SomeSing opt -> case toSing pre' of
      SomeSing pre -> case toSing tbl' of
        SomeSing tbl -> SomeSing $ STable opt pre tbl

-- | Typeless term-level 'Table' without optionality flag to construct name
-- synonym maps at run-time.
data TableId = TableId
  { tiPre  :: String
  , tiName :: String
  } deriving (Eq, Ord, Show)

getTableId
  :: forall (table :: Table)
   . Sing table
  -> TableId
getTableId t = case fromSing t of
  TTable _ pre tname -> TableId pre tname

getTableName
  :: forall (table :: Table)
   . Sing table
  -> QualifiedIdentifier
getTableName = fromString . tiName . getTableId

type family MakeOptional (t :: [Table]) :: [Table] where
  MakeOptional '[] = '[]
  MakeOptional (('Table o pre t) ': rest) =
    (('Table 'Opt pre t) ': (MakeOptional rest))

type family MakeMandatory (t :: [Table]) :: [Table] where
  MakeMandatory '[] = '[]
  MakeMandatory (('Table o pre t) ': rest) =
    (('Table 'Mnd pre t) ': (MakeMandatory rest))


-- | Map from table to it's synonym. Used to generate synonyms at run-time.
type SynonymsMap = M.Map TableId Text

lookupSyn :: TableId -> SynonymsMap -> Text
lookupSyn t sm = case M.lookup t sm of
  Nothing  -> error "Not found synonym for table. This is a bug!"
  Just syn -> syn

getTableIds :: forall (tbls :: [Table])
             . Sing tbls
            -> [TableId]
getTableIds = \case
  SNil -> []
  SCons tbl rest -> getTableId tbl : getTableIds rest

getColNames :: forall (cols :: [Symbol])
             . Sing cols
            -> [Identifier]
getColNames = \case
  SNil -> []
  SCons col rest -> (Identifier $ T.pack $ fromSing col) : getColNames rest

genSynonyms :: Sing (tbls :: [Table]) -> SynonymsMap
genSynonyms tbls = M.fromList $ zip (getTableIds tbls) generateSynonyms

genSynonymsSingle :: forall proxy (tbl :: Table)
                   . (SingI '[tbl])
                  => proxy tbl
                  -> SynonymsMap
genSynonymsSingle _ = genSynonyms (sing :: Sing '[tbl])

--  FIXME: make it infinite
generateSynonyms :: [Text]
generateSynonyms = map (T.pack . pure) ['a'..'z']
