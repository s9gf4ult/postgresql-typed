module Database.PostgreSQL.Typed.Query.Function where

import Data.Proxy
import Data.Singletons
import Database.PostgreSQL.Query
import Database.PostgreSQL.Typed.Expression
import Database.PostgreSQL.Typed.Query.Row

pgSelect :: (MonadPostgres m, FromRow (Row typs))
         => Select tbls typs
         -> m [Row typs]
pgSelect = pgQuery . renderSelect

-- pgUpdate :: (MonadPostgres m, FromRow (Row typs))
--          => Update tbl typs
--          -> m [Row typs]
-- pgUpdate = pgQuery . renderUpdate

pgSelectRow
  :: forall m row otables scope
   . ( FromTypedRow row otables, MonadPostgres m, SingI otables
     , ToSelectScope scope, otables ~ ScopeTables scope )
  => scope
  -> m [row]
pgSelectRow scope =
  fmap (map (unpackRow (Proxy :: Proxy row) (Proxy :: Proxy otables)))
  $ pgQuery $ renderSelect selectQ
  where
    selectQ = Select rows $ toSelectScope scope
    rows   = rowExpression (Proxy :: Proxy row)
