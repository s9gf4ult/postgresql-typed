module Database.PostgreSQL.Typed.Query.Row where

import Data.Proxy
import Data.Vinyl
import Data.Vinyl.Functor
import Database.PostgreSQL.Query
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Typed.Expression.Value
import Database.PostgreSQL.Typed.Table
import TypeFun.Data.List

newtype Row (typs :: [*]) = Row (HList typs)

instance FromRow (Row '[]) where
  fromRow = return $ Row RNil

instance forall a (as :: [*])
       . (FromField a, FromRow (Row as))
      => FromRow (Row (a ': as)) where
  fromRow = do
    a <- field
    Row rest <- fromRow
    return $ Row (Identity a :& rest)

type family UnpackFunc r (typs :: [*]) :: * where
  UnpackFunc r '[] = r
  UnpackFunc r (a ': as) = a -> UnpackFunc r as

applyTypedRow :: UnpackFunc r typs -> Row typs -> r
applyTypedRow f = \case
  Row (Identity a :& as) -> applyTypedRow (f a) $ Row as
  Row RNil      -> f

type family Result (f :: *) :: * where
  Result (a -> b) = Result b
  Result a = a

-- | Provide proof that result of some function is ... result of this function!
-- Yep
-- unpackResultEq :: (Result (UnpackFunc r typs) ~ r => a) -> a
-- unpackResultEq = error "FIXME: not implemented"

type family Mapped (f :: *) (x :: *) :: * where
  Mapped (a -> b) x = (a -> (Mapped b x))
  Mapped a x = x

class MapResult f x where
  mapResult :: (Result f -> x) -> f -> Mapped f x

instance {-# OVERLAPPING #-}
         (MapResult b x)
         => MapResult (a -> b) x where
  mapResult m f = fmap (mapResult m) f

-- | Instance for something that not a function
instance {-# OVERLAPPABLE #-}
         (Result a ~ a, Mapped a x ~ x)
         => MapResult a x where
  mapResult m a = m a

class (FromRow (Row (RowTypes row)))
      => FromTypedRow (row :: *) (otables :: [Table]) where
  type RowTypes row :: [*]
  fromTypedRow
    :: proxy1 row
    -> proxy2 otables
    -> UnpackFunc row (RowTypes row)
  rowExpression
    :: proxy row
    -> SomeExpressionList otables (RowTypes row)

instance forall a as otables
         . ( FromRow (Row (RowTypes a :++: RowTypes (HList as)))
           , FromTypedRow a otables
           , FromTypedRow (HList as) otables
           , MapResult (UnpackFunc a (RowTypes a)) (UnpackFunc (HList (a ': as)) (RowTypes (HList as)))
           , MapResult (UnpackFunc (HList as) (RowTypes (HList as))) (HList (a ': as))
           , UnpackFunc (HList (a ': as)) (RowTypes (HList (a ': as)))
             ~ Mapped (UnpackFunc a (RowTypes a)) (UnpackFunc (HList (a ': as)) (RowTypes (HList as)))
           , Mapped (UnpackFunc (HList as) (RowTypes (HList as))) (HList (a ': as))
             ~ UnpackFunc (HList (a ': as)) (RowTypes (HList as))
           , a ~ Result (UnpackFunc a (RowTypes a))
           , HList as ~ Result (UnpackFunc (HList as) (RowTypes (HList as)))
           )
         => FromTypedRow (HList (a ': as)) otables where
  type RowTypes (HList (a ': as)) = RowTypes a :++: RowTypes (HList as)
  fromTypedRow _ op = mapResult fcons ftrh
    where
      fcons :: a -> UnpackFunc (HList (a ': as)) (RowTypes (HList as))
      fcons a =
        let
          f :: HList as -> HList (a ': as)
          f t = (Identity a) :& t
        in mapResult f ftrt
      ftrh :: UnpackFunc a (RowTypes a)
      ftrh = fromTypedRow (Proxy :: Proxy a) op
      ftrt :: UnpackFunc (HList as) (RowTypes (HList as))
      ftrt = fromTypedRow (Proxy :: Proxy (HList as)) op
  rowExpression _ = elconcat rh rt
    where
      rh = rowExpression (Proxy :: Proxy a)
      rt = rowExpression (Proxy :: Proxy (HList as))

instance FromTypedRow (HList '[]) otables where
  type RowTypes (HList '[]) = '[]
  fromTypedRow _ _ = RNil
  rowExpression _ = Narrowed ELNil

--  FIXME: Add more instances for tuples of more elements
instance ( FromTypedRow (HList '[a, b]) otables
         , MapResult (UnpackFunc (HList '[a, b]) (RowTypes (a, b))) (a, b)
         , UnpackFunc (a, b) (RowTypes (a, b))
           ~ Mapped (UnpackFunc (HList '[a, b]) (RowTypes (a, b))) (a, b)
         , Result (UnpackFunc (HList '[a, b]) (RowTypes (a, b)))
           ~ HList '[a, b] )
         => FromTypedRow (a, b) otables where
  type RowTypes (a, b) = RowTypes (HList '[a, b])
  fromTypedRow _ potables = mapResult f $ fromTypedRow prow potables
    where
      prow = Proxy :: Proxy (HList '[a, b])
      f :: HList '[a, b] -> (a, b)
      f = \case
        (Identity a :& Identity b :& RNil) -> (a, b)
        _                                  -> error "IMPOSIBUR"
  rowExpression _ = rowExpression (Proxy :: Proxy (HList '[a, b]))

instance ( FromTypedRow (HList '[a, b, c]) otables
         , MapResult (UnpackFunc (HList '[a, b, c]) (RowTypes (a, b, c))) (a, b, c)
         , UnpackFunc (a, b, c) (RowTypes (a, b, c))
           ~ Mapped (UnpackFunc (HList '[a, b, c]) (RowTypes (a, b, c))) (a, b, c)
         , Result (UnpackFunc (HList '[a, b, c]) (RowTypes (a, b, c)))
           ~ HList '[a, b, c] )
         => FromTypedRow (a, b, c) otables where
  type RowTypes (a, b, c) = RowTypes (HList '[a, b, c])
  fromTypedRow _ potables = mapResult f $ fromTypedRow prow potables
    where
      prow = Proxy :: Proxy (HList '[a, b, c])
      f :: HList '[a, b, c] -> (a, b, c)
      f = \case
        (Identity a :& Identity b :& Identity c :& RNil) -> (a, b, c)
        _                                                -> error "IMPOSIBUR"
  rowExpression _ = rowExpression (Proxy :: Proxy (HList '[a, b, c]))

instance ( FromTypedRow (HList '[a, b, c, d]) otables
         , MapResult (UnpackFunc (HList '[a, b, c, d]) (RowTypes (a, b, c, d))) (a, b, c, d)
         , UnpackFunc (a, b, c, d) (RowTypes (a, b, c, d))
           ~ Mapped (UnpackFunc (HList '[a, b, c, d]) (RowTypes (a, b, c, d))) (a, b, c, d)
         , Result (UnpackFunc (HList '[a, b, c, d]) (RowTypes (a, b, c, d)))
           ~ HList '[a, b, c, d] )
         => FromTypedRow (a, b, c, d) otables where
  type RowTypes (a, b, c, d) = RowTypes (HList '[a, b, c, d])
  fromTypedRow _ potables = mapResult f $ fromTypedRow prow potables
    where
      prow = Proxy :: Proxy (HList '[a, b, c, d])
      f :: HList '[a, b, c, d] -> (a, b, c, d)
      f = \case
        (Identity a :& Identity b :& Identity c :& Identity d :& RNil)
          -> (a, b, c, d)
        _ -> error "IMPOSIBUR"
  rowExpression _ = rowExpression (Proxy :: Proxy (HList '[a, b, c, d]))

unpackRow
  :: (FromTypedRow r otables)
  => proxy1 r
  -> proxy2 otables
  -> Row (RowTypes r)
  -> r
unpackRow rp op row =
  applyTypedRow (fromTypedRow rp op) row

class ToTuple els where
  type Tuple els :: *
  toTuple :: Row els -> Tuple els

instance ToTuple '[a, b] where
  type Tuple '[a, b] = (a, b)
  toTuple (Row r) = case r of
    Identity a :& Identity b :& RNil -> (a, b)
    _                                -> error "IMPOSIBUR"

instance ToTuple '[a, b, c] where
  type Tuple '[a, b, c] = (a, b, c)
  toTuple (Row r) = case r of
    Identity a :& Identity b :& Identity c :& RNil
      -> (a, b, c)
    _ -> error "IMPOSIBUR"

instance ToTuple '[a, b, c, d] where
  type Tuple '[a, b, c, d] = (a, b, c, d)
  toTuple (Row r) = case r of
    Identity a :& Identity b :& Identity c :& Identity d :& RNil
      -> (a, b, c, d)
    _ -> error "IMPOSIBUR"

instance ToTuple '[a, b, c, d, e] where
  type Tuple '[a, b, c, d, e] = (a, b, c, d, e)
  toTuple (Row r) = case r of
    Identity a :& Identity b :& Identity c :& Identity d :& Identity e :& RNil
      -> (a, b, c, d, e)
    _ -> error "IMPOSIBUR"

toTuples :: (ToTuple els) => [Row els] -> [Tuple els]
toTuples = map toTuple
