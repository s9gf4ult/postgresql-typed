module TableExample where

import Data.ByteString (ByteString)
import Data.Proxy
import Data.Singletons
import Data.Text (Text)
import Database.PostgreSQL.Query
import Database.PostgreSQL.Typed
import GHC.TypeLits
import TypeFun.Data.List

type instance ColType "users" "id" = Int
type instance ColType "users" "name" = Text

type instance ColType "posts" "id" = Int
type instance ColType "posts" "user_id" = Int
type instance ColType "posts" "text" = Text

type instance ColType "comments" "id" = Int
type instance ColType "comments" "text" = Text
type instance ColType "comments" "post_id" = Int
type instance ColType "comments" "user_id" = Int

type instance ColType "attachments" "id" = Int
type instance ColType "attachments" "attachable_id" = Int
type instance ColType "attachments" "attachable_type" = Text
type instance ColType "attachments" "body_type" = Text
type instance ColType "attachments" "body_id" = Int

type instance ColType "attachment_files" "id" = Int
type instance ColType "attachment_files" "name" = Text

type instance ColType "attachment_bs" "id" = Int
type instance ColType "attachment_bs" "body" = ByteString

data User (opt :: Optionality) (n :: Symbol) = User
  { uId   :: ColExprType ('Table opt n "users") "id"
  , uName :: ColExprType ('Table opt n "users") "name"
  }

deriving instance ( Eq (ColExprType ('Table opt n "users") "id")
                  , Eq (ColExprType ('Table opt n "users") "name") )
                  => Eq (User opt n)

instance ( SubList '[ 'Table opt pre "users"] otables
         , FromField (ColExprType ('Table opt pre "users") "id")
         , FromField (ColExprType ('Table opt pre "users") "name")
         , SingI opt
         , SingI pre
         , KnownSymbol pre )
         => FromTypedRow (User opt pre) otables where
  type RowTypes (User opt pre) =
    '[ ColExprType ('Table opt pre "users") "id"
     , ColExprType ('Table opt pre "users") "name" ]
  fromTypedRow _ _ = User
  rowExpression _ = Narrowed
    $ ECol tsing (sing :: Sing "id")
    ::& ECol tsing (sing :: Sing "name")
    ::& ELNil
    where
      tsing = sing :: Sing ('Table opt pre "users")

data Comment (n :: Symbol) = Comment
  { cId     :: Int
  , cText   :: Text
  , cPostId :: Int
  , cUserId :: Int
  } deriving (Eq, Show)

instance ( SubList '[ 'Table 'Mnd pre "comments"] otables
         , KnownSymbol pre )
         => FromTypedRow (Comment pre) otables where
  type RowTypes (Comment pre) = '[Int, Text, Int, Int]
  fromTypedRow _ _ = Comment
  rowExpression _ = Narrowed
    $ col (Proxy :: Proxy '( 'Mnd, pre, "comments", "id"))
    ::& col (Proxy :: Proxy '( 'Mnd, pre, "comments", "text"))
    ::& col (Proxy :: Proxy '( 'Mnd, pre, "comments", "post_id"))
    ::& col (Proxy :: Proxy '( 'Mnd, pre, "comments", "user_id"))
    ::& ELNil
