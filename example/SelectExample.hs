module SelectExample where

import Control.Lens
import Data.Text (Text)
import Database.PostgreSQL.Query
import Database.PostgreSQL.Typed
import TableExample

selectUser :: (MonadPostgres m) => Int -> m [User 'Mnd ""]
selectUser uid = do
  pgSelectRow $ col [c|users.id|] ==. litE uid

-- | Two LEFT JOIN with conditions Conditions in WHERE clause
selectPosted :: (MonadPostgres m) => Text -> m [User 'Mnd ""]
selectPosted txt = do
  pgSelectRow
    $ jImplicit (col [c|users.id|] ==. col [c|?posts.user_id|])
    #+# col [c|users.id|] ==. col [c|?comments.user_id|]
    #+# col [c|users.id|] ==. col [c|comments.user_id|] --  FIXME: must not compile!
    & set sbCondition (col [c|?posts.text|] `likeE` litE txt
                       ||. col [c|?comments.text|] `likeE` litE txt)

-- | Select users commented posts of some user, except user itself. The result
-- is CROSS JOIN over 4 tables and WHERE clause
commenters :: (MonadPostgres m) => Int -> m [User 'Mnd "commenter"]
commenters uid = do
  pgSelectRow
    $ col [c|author|users.id|] ==. litE uid
    &&. col [c|author|users.id|] ==. col [c|posts.user_id|]
    &&. col [c|posts.id|] ==. col [c|comments.post_id|]
    &&. col [c|comments.user_id|] ==. col [c|commenter|users.id|]
    &&. col [c|commenter|users.id|] /=. litE uid

userAttachedFiles :: (MonadPostgres m) => Int -> m [Row '[Text]]
userAttachedFiles uid = do
  pgSelect $ toSelect (col [c|attachment_files.name|] ::& ELNil)
    $ col [c|users.id|] ==. litE uid
    &&. col [c|users.id|] ==. col [c|posts.user_id|]
    &&. col [c|posts.id|] ==. col [c|attachments.attachable_id|]
    &&. col [c|attachments.attachable_type|] ==. textE "posts"
    &&. col [c|attachment_files.id|] ==. col [c|attachments.body_id|]
    &&. col [c|attachments.body_type|] ==. textE "file"
