BEGIN ;

CREATE TABLE users
    ( "id" serial primary key
    , "name" text not null
    , "password" text
    , "confirmed_at" timestamptz
    , "created_at" timestamptz not null default now()
    ) ;

CREATE TABLE posts
    ( "id" serial primary key
    , "user_id" integer not null references users(id)
    , "content" text not null
    , "created_at" timestamptz not null default now()
    ) ;

CREATE TABLE comments
    ( "id" serial primary key
    , "user_id" integer not null references users(id)
    , "post_id" integer not null references posts(id)
    , "content" text not null
    , "created_at" timestamptz not null default now()
    ) ;

COMMIT ;
