module Main where

import Control.Monad.Logger
import Database.PostgreSQL.Query
import SelectExample
import TableExample

--  FIXME: Run all example functions here to demostrate their workability
launch :: (MonadPostgres m) => m ()
launch = return ()

main :: IO ()
main = do
  c <- connectPostgreSQL "user=test dbname=test"
  runStderrLoggingT $ runPgMonadT c launch
