import Data.Proxy
import Data.Text (Text)
import Data.Vinyl
import Database.PostgreSQL.Query
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Typed.Expression

con <- connectPostgreSQL "user=postgres"
let renderQ q = runSqlBuilder con q

let e = sref (Proxy :: Proxy String) "u" "name" :& sref (Proxy :: Proxy Int) "u" "id" :& RNil

let f = [fetable "users" "u"]

let w = (sref (Proxy :: Proxy Text) "u" "name" ==. ELit "vano") &&. (sref (Proxy :: Proxy Int) "u" "id" /=. ELit 10)
